<?php
/* Smarty version 3.1.34-dev-7, created on 2020-07-09 23:50:52
  from 'C:\xampp\htdocs\todojuegos\templates\tabla.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5f07913c1a9ab2_57618189',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a25832ce0e562be9e2640ff9225ec101d7ce3204' => 
    array (
      0 => 'C:\\xampp\\htdocs\\todojuegos\\templates\\tabla.tpl',
      1 => 1594139540,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:barradenavegacion.tpl' => 1,
    'file:piedepagina.tpl' => 1,
  ),
),false)) {
function content_5f07913c1a9ab2_57618189 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender('file:barradenavegacion.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<div class="row align-items-stretch contenedorficha ">

  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['fichas']->value, 'ficha');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['ficha']->value) {
?>
  
    <div class="col-sm-2 fichainicio ">        
      <div class="card ">           
        <div class="ficha">
          <img src="<?php echo $_smarty_tpl->tpl_vars['ficha']->value->img;?>
" class="card-img" alt="">   
          <div class="contenidoficha">
            <h5 class="card-title"><?php echo $_smarty_tpl->tpl_vars['ficha']->value->titulo;?>
</h5>
            <h6><?php echo $_smarty_tpl->tpl_vars['ficha']->value->categoria_titulo;?>
</h6>
            <h5 class="card-title"></h5>
            <a href="fichajuegos/<?php echo $_smarty_tpl->tpl_vars['ficha']->value->id_ficha;?>
" class="btn btn-primary">Ver mas</a>
          </div>  
        </div>
      </div>
    </div>
  <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</div>


<?php $_smarty_tpl->_subTemplateRender('file:piedepagina.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
