<?php
/* Smarty version 3.1.34-dev-7, created on 2020-07-09 20:51:48
  from 'C:\xampp\htdocs\todojuegos\templates\fichajuego.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5f076744916391_28286379',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '07e728dd4c0acae0108db020cf1d26955e684651' => 
    array (
      0 => 'C:\\xampp\\htdocs\\todojuegos\\templates\\fichajuego.tpl',
      1 => 1593978858,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:barradenavegacion.tpl' => 1,
    'file:vue/comentarios.tpl' => 1,
    'file:piedepagina.tpl' => 1,
  ),
),false)) {
function content_5f076744916391_28286379 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender('file:barradenavegacion.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<div class="contenedorjuego">
  <div class="card juego ">
    <img src="<?php echo $_smarty_tpl->tpl_vars['juego']->value->img;?>
" class="card-img-top" alt="<?php echo $_smarty_tpl->tpl_vars['juego']->value->titulo;?>
">
    <div class="card-body">
      <h5 class="card-title "><?php echo $_smarty_tpl->tpl_vars['juego']->value->titulo;?>
</h5>
      <p class="card-text"><?php echo $_smarty_tpl->tpl_vars['juego']->value->descripcion;?>
</p>
    </div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item">Categoria : <?php echo $_smarty_tpl->tpl_vars['juego']->value->categoria_titulo;?>
 </li> 
      <li class="list-group-item">Placa de video: <?php echo $_smarty_tpl->tpl_vars['juego']->value->so;?>
 </li>
      <li class="list-group-item">sistema operativo: <?php echo $_smarty_tpl->tpl_vars['juego']->value->graficos;?>
</li>
      <li class="list-group-item">Espacio en disco: <?php echo $_smarty_tpl->tpl_vars['juego']->value->espacio;?>
</li>
    </ul>
    <div class="card-body">
      <a href="<?php echo $_smarty_tpl->tpl_vars['juego']->value->link;?>
" class="card-link">Descargar</a>
    </div>
  </div>
</div>

<?php $_smarty_tpl->_subTemplateRender('file:vue/comentarios.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<input value="<?php echo $_smarty_tpl->tpl_vars['usuario']->value['id_usuario'];?>
" id="dato" type="hidden">
<input value="<?php echo $_smarty_tpl->tpl_vars['usuario']->value['nombre'];?>
" id="dato2" type="hidden">
<input value="<?php echo $_smarty_tpl->tpl_vars['juego']->value->id_ficha;?>
" id="id_juego" type="hidden">

<?php echo '<script'; ?>
 src="js/comentarios.js"><?php echo '</script'; ?>
>
<?php $_smarty_tpl->_subTemplateRender('file:piedepagina.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
