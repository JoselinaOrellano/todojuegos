<?php
/* Smarty version 3.1.34-dev-7, created on 2020-07-09 20:09:16
  from 'C:\xampp\htdocs\todojuegos\templates\tablaJuegos.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5f075d4c0f99f2_21685742',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '645bfbfb76989fdd496f368eeeb33b818b92d61f' => 
    array (
      0 => 'C:\\xampp\\htdocs\\todojuegos\\templates\\tablaJuegos.tpl',
      1 => 1592918802,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:barradenavegacion.tpl' => 1,
    'file:piedepagina.tpl' => 1,
  ),
),false)) {
function content_5f075d4c0f99f2_21685742 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender('file:barradenavegacion.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div class="row contenedorficha ">
  <table class="table table-striped table-dark">
    <thead>
      <tr>
        <th scope="col">Juego</th>
        <th scope="col">Categoría</th>
        <th scope="col">Eliminar</th>
        <th scope="col">Modificar</th>
      </tr>
    </thead>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['fichas']->value, 'ficha');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['ficha']->value) {
?>
      <tbody>
        <tr>
          <th scope="row">
            <a href="fichajuegos/<?php echo $_smarty_tpl->tpl_vars['ficha']->value->id_ficha;?>
" class=""><?php echo $_smarty_tpl->tpl_vars['ficha']->value->titulo;?>
</a>
          </th>
          <td><?php echo $_smarty_tpl->tpl_vars['ficha']->value->categoria_titulo;?>

          </td>
          <td>
            <a type="button" href="eliminarjuego/<?php echo $_smarty_tpl->tpl_vars['ficha']->value->id_ficha;?>
" class="btn btn-danger" onclick="return confirmarEliminacion()">Eliminar</a>
          </td>
          <td> 
            <a type="button" href="modificarjuego/<?php echo $_smarty_tpl->tpl_vars['ficha']->value->id_ficha;?>
" class="btn btn-primary">Modificar</a>
          </td>
        </tr>
      </tbody>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
  </table>
</div>

<?php echo '<script'; ?>
 type="text/javascript">
  function confirmarEliminacion(){
    var respuesta = confirm("¿Estas seguro que deseas eliminar este juego?" );
    if(respuesta == true){
      return true;
    } 
    else{
      return false;
    }
  }
<?php echo '</script'; ?>
>

<?php $_smarty_tpl->_subTemplateRender('file:piedepagina.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
