<?php
/* Smarty version 3.1.34-dev-7, created on 2020-07-09 20:09:52
  from 'C:\xampp\htdocs\todojuegos\templates\tablaCategorias.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5f075d70b98733_71505946',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '45bee6cca3e2d545f3b998da11425e936db69c70' => 
    array (
      0 => 'C:\\xampp\\htdocs\\todojuegos\\templates\\tablaCategorias.tpl',
      1 => 1592868044,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:barradenavegacion.tpl' => 1,
    'file:piedepagina.tpl' => 1,
  ),
),false)) {
function content_5f075d70b98733_71505946 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender('file:barradenavegacion.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div class="row contenedorficha ">
  <table class="table table-striped table-dark">
    <thead>
      <tr>
        <th scope="col">Categoría</th>
        <th scope="col">Eliminar</th>
        <th scope="col">Modificar</th>
      </tr>
    </thead>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categorias']->value, 'categoria');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['categoria']->value) {
?>
      <tbody>
        <tr>
          <th scope="row">
            <a href="filtrar/<?php echo $_smarty_tpl->tpl_vars['categoria']->value->id_categoria;?>
" class=""><?php echo $_smarty_tpl->tpl_vars['categoria']->value->titulo;?>
</a>
          </th>
          <td>
            <a type="button" href="eliminarcategoria/<?php echo $_smarty_tpl->tpl_vars['categoria']->value->id_categoria;?>
" class="btn btn-danger" onclick="return confirmarEliminacion()">Eliminar</a>
          </td>
          <td> 
            <a type="button" href="modificarcategoria/<?php echo $_smarty_tpl->tpl_vars['categoria']->value->id_categoria;?>
" class="btn btn-primary">Modificar</a>
          </td>
        </tr>
      </tbody>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
  </table>
</div>

<?php echo '<script'; ?>
 type="text/javascript">
  function confirmarEliminacion(){
    var respuesta = confirm("¿Estas seguro que deseas eliminar esta categoria y todos los juegos asociados a ella?");
    if(respuesta == true){
      return true;
    } 
    else{
      return false;
    }
  }
<?php echo '</script'; ?>
>

<?php $_smarty_tpl->_subTemplateRender('file:piedepagina.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
