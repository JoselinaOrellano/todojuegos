<?php
/* Smarty version 3.1.34-dev-7, created on 2020-07-09 21:00:10
  from 'C:\xampp\htdocs\todojuegos\templates\barradenavegacion.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5f07693a5e99c2_41478009',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '242bde6096c0bc8ada77bab7e109ff2668d84a2e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\todojuegos\\templates\\barradenavegacion.tpl',
      1 => 1594321051,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f07693a5e99c2_41478009 (Smarty_Internal_Template $_smarty_tpl) {
?><nav class="navbar navbar-expand-lg navbar navbar-dark bg-dark">
        <a class="navbar-brand" href="home"><h1>Todo juegos</h1></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="home">Inicio <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle"  id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Categorias
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categorias']->value, 'categoria');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['categoria']->value) {
?>
                  <a class="dropdown-item" href="listadoCategorias/<?php echo $_smarty_tpl->tpl_vars['categoria']->value->id_categoria;?>
"> <?php echo $_smarty_tpl->tpl_vars['categoria']->value->titulo;?>
 </a> 
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>                
              </div>
            </li>
          </ul>

          <?php if ($_smarty_tpl->tpl_vars['usuario']->value['nombre'] == 'no') {?>
            <div class="form-inline my-2 my-lg-0">
              <a href="ingresar" class="btn btn-outline-primary my-2 my-sm-0 ">Ingresar</a>
              <a href="registrarse" class="btn btn-outline-primary my-2 my-sm-0 ">Registrarse</a>              
            </div>
            <input value="2" id="registro" type="hidden">

             <?php } elseif ($_smarty_tpl->tpl_vars['usuario']->value['permiso'] == '1') {?>                
                <div class="btn-group">
                  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo $_smarty_tpl->tpl_vars['usuario']->value['nombre'];?>

                  </button>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="cargarjuego">Cargar Juego</a>
                  <a class="dropdown-item" href="listadojuegos/todos">Modificar Juego</a>
                  <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="cargarcategoria">Cargar Categoria</a>
                    <a class="dropdown-item" href="listadoCategorias/todos">Modificar Categoria</a>
                  <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="administrarUsuarios"><span>Administrar Usuarios</span></a>
                  <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="cerrarsesion"><span>Cerrar sesion</span></a>
                </div>
                <input value="1" id="registro" type="hidden">
            <?php } else { ?>
              <div class="btn-group">
                <button type="button" class="btn btn-primary " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <?php echo $_smarty_tpl->tpl_vars['usuario']->value['nombre'];?>

                </button>
                
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="cerrarsesion"><span>Cerrar sesion</span></a>
              </div>       
              <input value="0" id="registro" type="hidden">         
          <?php }?>
          
        </div>
        
      </nav>
    <?php }
}
