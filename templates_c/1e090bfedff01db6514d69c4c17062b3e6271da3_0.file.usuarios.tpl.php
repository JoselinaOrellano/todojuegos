<?php
/* Smarty version 3.1.34-dev-7, created on 2020-07-10 19:15:09
  from 'C:\xampp\htdocs\todojuegos\templates\usuarios.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5f08a21d925e85_56243609',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1e090bfedff01db6514d69c4c17062b3e6271da3' => 
    array (
      0 => 'C:\\xampp\\htdocs\\todojuegos\\templates\\usuarios.tpl',
      1 => 1594392402,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:barradenavegacion.tpl' => 1,
    'file:piedepagina.tpl' => 1,
  ),
),false)) {
function content_5f08a21d925e85_56243609 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender('file:barradenavegacion.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<div class="contenedorUsuarios">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['usuarios']->value, 'usuario');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['usuario']->value) {
?>
        <div class="card text-white border-primary mb-3 tarjetaUsuarios" >
            <div class="card-header emailUsuario"><?php echo $_smarty_tpl->tpl_vars['usuario']->value->email;?>
</div>
            <div class="card-body">
                <h4 class="card-title"><?php echo $_smarty_tpl->tpl_vars['usuario']->value->nombre;?>
</h4>
                <p class="card-text textoUsuario">Permiso:                 
                    <?php if ($_smarty_tpl->tpl_vars['usuario']->value->permiso == 1) {?>
                        administrador
                        <?php } else { ?>
                        usuario
                    <?php }?>
                </p>
            </div>
            <div>
                <?php if ($_smarty_tpl->tpl_vars['usuario']->value->permiso == 1) {?>
                    <a href="cambiarpermiso/<?php echo $_smarty_tpl->tpl_vars['usuario']->value->id_usuario;?>
/<?php echo $_smarty_tpl->tpl_vars['usuario']->value->permiso;?>
" type="button" class="btn btn-dark ">Quitar permiso </a>
                <?php } else { ?>
                    <a href="cambiarpermiso/<?php echo $_smarty_tpl->tpl_vars['usuario']->value->id_usuario;?>
/<?php echo $_smarty_tpl->tpl_vars['usuario']->value->permiso;?>
" class="btn btn-dark ">Otorgar permiso</a>
                <?php }?>  
               
                <a href="eliminarUsuario/<?php echo $_smarty_tpl->tpl_vars['usuario']->value->id_usuario;?>
" type="button" class="btn btn-dark ">Eliminar usuario</a>
            </div>
        </div>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</div>

<?php $_smarty_tpl->_subTemplateRender('file:piedepagina.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
