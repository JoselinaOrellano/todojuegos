{literal}

<div class="contenedorComentarios" id="appVue">
  <h1 class="comentarios">Comentarios</h1>
  <div v-if="registro==1||registro==0" >
    <div>
      <h5 class="tituloingresarcomentario">Escribe tu comentario:</h5>
    </div>
    <div class="input-group escribirComentario">  
      <div class="ingresartexto">
        <textarea class="form-control" id="contenido" aria-label="With textarea"></textarea>
      </div>
      <div class="cajapuntaje">
        <label class="my-1 mr-2 " for="inlineFormCustomSelectPref">Puntaje</label>
        <select class="custom-select mr-sm-2" id="puntaje">
          <option selected>Elegir...</option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select>
      </div>
      <button type="button" v-on:click="enviar" class="btn btn-primary btn-lg  btnComentarios">Enviar comentario</button>
    </div>
  </div>

  <div v-for="comentario in comentarios" class="cajacomentarios">
    <div class="card ">
      <div class="card-header">
       <h5> {{comentario.nombre_usuario}}</h5>
      </div>
      <div class="card-body">
        <blockquote class="blockquote mb-0">
          <p>{{comentario.comentario}}</p>   
          <p>Puntaje: {{comentario.puntaje}}</p>    
          <button  v-if="registro==1" v-on:click="eliminarComentario(comentario.id_comentario)" type="button" class="btn btn-danger" id="eliminar">Eliminar</button>
        </blockquote>
      </div>
    </div>
  </div>

</div>



{/literal}

