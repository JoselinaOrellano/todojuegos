{include 'header.tpl'}
{include 'barradenavegacion.tpl'}


<div class="row contenedorficha ">
  <table class="table table-striped table-dark">
    <thead>
      <tr>
        <th scope="col">Juego</th>
        <th scope="col">Categoría</th>
        <th scope="col">Eliminar</th>
        <th scope="col">Modificar</th>
      </tr>
    </thead>
    {foreach $fichas item=ficha }
      <tbody>
        <tr>
          <th scope="row">
            <a href="fichajuegos/{$ficha->id_ficha}" class="">{$ficha->titulo}</a>
          </th>
          <td>{$ficha->categoria_titulo}
          </td>
          <td>
            <a type="button" href="eliminarjuego/{$ficha->id_ficha}" class="btn btn-danger" onclick="return confirmarEliminacion()">Eliminar</a>
          </td>
          <td> 
            <a type="button" href="modificarjuego/{$ficha->id_ficha}" class="btn btn-primary">Modificar</a>
          </td>
        </tr>
      </tbody>
    {/foreach}
  </table>
</div>

<script type="text/javascript">
  function confirmarEliminacion(){
    var respuesta = confirm("¿Estas seguro que deseas eliminar este juego?" );
    if(respuesta == true){
      return true;
    } 
    else{
      return false;
    }
  }
</script>

{include 'piedepagina.tpl'}

