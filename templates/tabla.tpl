{include 'header.tpl'}
{include 'barradenavegacion.tpl'}

<div class="row align-items-stretch contenedorficha ">

  {foreach $fichas item=ficha }
  
    <div class="col-sm-2 fichainicio ">        
      <div class="card ">           
        <div class="ficha">
          <img src="{$ficha->img}" class="card-img" alt="">   
          <div class="contenidoficha">
            <h5 class="card-title">{$ficha->titulo}</h5>
            <h6>{$ficha->categoria_titulo}</h6>
            <h5 class="card-title"></h5>
            <a href="fichajuegos/{$ficha->id_ficha}" class="btn btn-primary">Ver mas</a>
          </div>  
        </div>
      </div>
    </div>
  {/foreach}
</div>


{include 'piedepagina.tpl'}