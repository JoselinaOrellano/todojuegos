{include 'header.tpl'}
{include 'barradenavegacion.tpl'}

<div class="contenedorUsuarios">
    {foreach $usuarios item=usuario }
        <div class="card text-white border-primary mb-3 tarjetaUsuarios" >
            <div class="card-header emailUsuario">{$usuario->email}</div>
            <div class="card-body">
                <h4 class="card-title">{$usuario->nombre}</h4>
                <p class="card-text textoUsuario">Permiso:                 
                    {if $usuario->permiso== 1}
                        administrador
                        {else}
                        usuario
                    {/if}
                </p>
            </div>
            <div>
                {if $usuario->permiso== 1}
                    <a href="cambiarpermiso/{$usuario->id_usuario}/{$usuario->permiso}" type="button" class="btn btn-dark ">Quitar permiso </a>
                {else}
                    <a href="cambiarpermiso/{$usuario->id_usuario}/{$usuario->permiso}" class="btn btn-dark ">Otorgar permiso</a>
                {/if}  
               
                <a href="eliminarUsuario/{$usuario->id_usuario}" type="button" class="btn btn-dark ">Eliminar usuario</a>
            </div>
        </div>
    {/foreach}
</div>

{include 'piedepagina.tpl'}