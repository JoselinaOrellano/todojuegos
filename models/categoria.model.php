<?php
class ModeloCategoria{    
    private function crearConexion() {
        $host = 'localhost';
        $nombreDeUsuario = 'root';
        $contraseña = '';
        $baseDeDatos = 'db_fichas';
        try {
            $pdo = new PDO("mysql:host=$host;dbname=$baseDeDatos;charset=utf8", $nombreDeUsuario, $contraseña);
        } catch (Exception $e) {
            var_dump($e);
        }
        return $pdo;
    }

    function traerCategorias() {//traigo todas las  categorias.
        $db = $this->crearConexion();        
        $query = $db->prepare("SELECT * FROM categorias");
        $query->execute();
        $categorias = $query->fetchAll(PDO::FETCH_OBJ);

        return $categorias;
    } 
   
    public function  eliminarCategoria($id_categoria){
        $db = $this->crearConexion();
        $sentencia = $db->prepare("DELETE FROM categorias WHERE id_categoria = ?"); 
        $sentencia->execute([$id_categoria]); 
        
        $this->eliminarJuegosCategotia($id_categoria);
    }

    public function eliminarJuegosCategotia($id_categoria){
        $db = $this->crearConexion();
        $sentencia = $db->prepare("DELETE FROM ficha WHERE id_categoria = ?"); 
        $sentencia->execute([$id_categoria]);
    }

    public function traerCategoria($id_categoria){
        $db = $this->crearConexion();
        $sentencia = $db->prepare("SELECT * FROM categorias WHERE id_categoria = ?");
        $sentencia->execute([$id_categoria]);
        return $sentencia->fetch(PDO::FETCH_OBJ);
    }

    public function  modificarCategoria($titulo, $categoria){
        $db = $this->crearConexion();
        $sentencia = $db->prepare("UPDATE categorias SET titulo = ? WHERE categorias.id_categoria= ? "); 
        $sentencia->execute([$titulo, $categoria]);
    }
    

}