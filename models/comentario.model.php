<?php
class ComentariosModelo{
    private $db;

    public function __construct() {
        $this->db = $this->crearConexion();
    }

    private function crearConexion() {
        $host = 'localhost';
        $nombreDeUsuario = 'root';
        $contraseña = '';
        $baseDeDatos = 'db_fichas';
        try {
        $pdo = new PDO("mysql:host=$host;dbname=$baseDeDatos;charset=utf8", $nombreDeUsuario, $contraseña);
        } catch (Exception $e) {
            var_dump($e);
        }
        return $pdo;
    }

    public function obtenerComentarios($juego){
        $db = $this->crearConexion();
        $sentencia = $db->prepare("SELECT comentarios.*, usuarios.email as nombre_usuario FROM comentarios, usuarios WHERE (comentarios.id_usuario = usuarios.id_usuario AND comentarios.id_juego = ? )");
        $sentencia->execute([$juego]);
        $comentarios = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $comentarios;
    }

    public function agregarComentario($juego, $usuario, $comentario, $puntaje){
        $db = $this->crearConexion();
        $sentencia = $db->prepare("INSERT INTO comentarios(id_juego, id_usuario, comentario, puntaje)
         VALUES(?, ?, ?, ?)"); 
        $sentencia->execute([$juego, $usuario, $comentario, $puntaje]); 
    }
    
    public function eliminarComentario($comentario){
        try {
            error_reporting(0);
            $db = $this->crearConexion();
            $sentencia = $db->prepare("DELETE FROM comentarios WHERE id_comentario = ?"); 
            $sentencia->execute([$comentario]);
            return true;
        }
        catch(Exception $e){
            return false;
        } 

    }
}