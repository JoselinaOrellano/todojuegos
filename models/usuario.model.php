<?php

class ModeloUsuario {

    private $db;

    public function __construct() {
        $this->db = $this->crearConexion();
    }

     /* Crea la conexion*/
    private function crearConexion() {
        //listo en variables los datos para abrir la coneccion.
        $host = 'localhost';
        $nombreDeUsuario = 'root';
        $contraseña = '';
        $baseDeDatos = 'db_fichas';
        try {
        $pdo = new PDO("mysql:host=$host;dbname=$baseDeDatos;charset=utf8", $nombreDeUsuario, $contraseña);
        } catch (Exception $e) {
            var_dump($e);
        }
        return $pdo;
    }

    public function traerUsuario($email) {
        $sentencia = $this->db->prepare("SELECT * FROM usuarios WHERE email = ?");
        $sentencia->execute([$email]);
        return $sentencia->fetch(PDO::FETCH_OBJ);
    }

    public function traerUsuarios() {
        $sentencia = $this->db->prepare("SELECT * FROM usuarios");
        $sentencia->execute([]);
        return $sentencia->fetchAll(PDO::FETCH_OBJ);
    }

    public function guardarUsuario($email, $nombre, $contraseña, $permiso){
        $db = $this->crearConexion();
        $sentencia = $db->prepare("INSERT INTO usuarios(email, nombre, contraseña, permiso)
         VALUES(?, ?, ?, ?)"); 
        $sentencia->execute([$email, $nombre, $contraseña, $permiso]); 
    }

    public function cambiarPermisoUsuario($permiso, $usuario){
        $sentencia = $this->db->prepare("UPDATE usuarios SET permiso = ? WHERE usuarios.id_usuario = ?");
        $sentencia->execute([$permiso, $usuario]);
    }

    public function eliminarUsuario($usuario){
        $db = $this->crearConexion();
        $sentencia = $db->prepare("DELETE FROM usuarios WHERE id_usuario = ?"); 
        $sentencia->execute([$usuario]);  
    }

}