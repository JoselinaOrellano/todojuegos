<?php
require_once "controllers/formulario.controller.php";
class ModeloFicha{

//--------------------------------------------------------------------------

    private function crearConexion() {
        //listo en variables los datos para abrir la coneccion.
        $host = 'localhost';
        $nombreDeUsuario = 'root';
        $contraseña = '';
        $baseDeDatos = 'db_fichas';
        try {
        $pdo = new PDO("mysql:host=$host;dbname=$baseDeDatos;charset=utf8", $nombreDeUsuario, $contraseña);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            $error=new ControladorFormulario;
            $error->error();
        }
        return $pdo;
    }

//--------------------------------------------------------------------------

    public function traerFicha($juego){
        $db = $this->crearConexion();
        $sentencia = $db->prepare("SELECT ficha.*, categorias.titulo as categoria_titulo FROM ficha, categorias WHERE id_ficha = ?");
        $sentencia->execute([$juego]);
        $ficha = $sentencia->fetch(PDO::FETCH_OBJ);
        $requisitos= explode('/',$ficha->requisitos);
        $ficha->so=$requisitos[0];
        $ficha->graficos=$requisitos[1];
        $ficha->espacio=$requisitos[2];
        return $ficha;
    }

//----------------------------------------------------------------------
    public function traerTodasFichas() { 
        $db = $this->crearConexion();
        $sql="SELECT ficha.*, categorias.titulo as categoria_titulo FROM ficha, categorias 
        WHERE ficha.id_categoria=categorias.id_categoria";
        $query = $db->prepare($sql);
        $query->execute();
        $fichas = $query->fetchAll(PDO::FETCH_OBJ);       
        return $fichas;        
    }

//---------------------------------------------------------------------
    
    public function insertarFicha($titulo, $descripcion, $categoria, $requisitos, $imagen=null, $link) {        
        $db = $this->crearConexion();
        $sentencia = $db->prepare("INSERT INTO ficha(titulo, descripcion, requisitos, img, link, id_categoria)
         VALUES(?, ?, ?, ?, ?, ?)"); 
        $sentencia->execute([$titulo, $descripcion, $requisitos, $imagen, $link, $categoria]); 
    }

    public function copiarImagen()
    {
        // Nombre archivo original
        $nombreOriginal = $_FILES['imagen']['name'];
        // Nombre en el file system:
        $nombreFisico = $_FILES['imagen']['tmp_name'];
        
        $nombreFinal = "imagen/". uniqid("", true) . "." 
        . strtolower(pathinfo($nombreOriginal, PATHINFO_EXTENSION));

        move_uploaded_file($nombreFisico, $nombreFinal); 
        
        return $nombreFinal;
    }

    public function insertarConImagen($titulo, $descripcion, $categoria, $requisitos, $link)
    {
        $imagen = $this->copiarImagen();
        $success = $this->insertarFicha($titulo, $descripcion, $categoria, $requisitos, $imagen, $link);
        return $success;
    }

//--------------------------------------------------------------------------

    public function modificarConImagen($titulo, $descripcion, $categoria, $requisitos, $link, $ficha){
        $nombreFinal = $this->copiarImagen();
        $success = $this-> modificarJuego($titulo, $descripcion, $categoria, $requisitos, $link, $ficha, $nombreFinal);
        return $success;
    }


    public function modificarJuego($titulo, $descripcion, $categoria, $requisitos, $imagen=null, $link, $ficha) {
        $db = $this->crearConexion();
        $sentencia = $db->prepare("UPDATE ficha SET titulo = ? , descripcion = ? , 
        requisitos = ? , img = ? , link = ?, id_categoria = ? WHERE ficha.id_ficha = ? "); 
        $sentencia->execute([$titulo, $descripcion, $requisitos, $imagen, $link, $categoria, $ficha]); 
    }

//----------------------------------------------------------------------------

    public function eliminarJuego($id_juego){
        $db = $this->crearConexion();
        $sentencia = $db->prepare("DELETE FROM ficha WHERE id_ficha = ?"); 
        $sentencia->execute([$id_juego]);   
    }

  //----------------------------------------------------------------------------
  
    public function insertarCategoria($categoria){
        $db = $this->crearConexion();
        $sentencia = $db->prepare("INSERT INTO categorias(titulo) VALUES(?)");
        $sentencia->execute([$categoria]); 
    }

//----------------------------------------------------------------------------

    public function traerFichasPorCategoria($id_categoria){
        $db = $this->crearConexion();
        $sentencia = $db->prepare("SELECT ficha.*,categorias.titulo as categoria_titulo FROM ficha, categorias WHERE (ficha.id_categoria=categorias.id_categoria AND ficha.id_categoria= ?)");
        $sentencia->execute([$id_categoria]);
        $juegos=$sentencia->fetchAll(PDO::FETCH_OBJ);
        return $juegos;

    }

}
