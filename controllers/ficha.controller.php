<?php
require_once 'proyecto.controler.php';
require_once 'models/categoria.model.php';
require_once 'models/ficha.model.php';//enlazo con archivo de modelo.
require_once 'views/ficha.view.php';//enlazo con archivo de vista.

class ControladorFicha extends ControladorPadre {

    public function subirFicha() {
        $this->VerificarRegistro();

        $titulo = $_POST['titulo'];
        $descripcion = $_POST['descripcion'];
        $so = $_POST['so'];
        $graficos = $_POST['grafico'];
        $espacio =$_POST['espacio'];
        $link = $_POST['link'];
        $categoria = $_POST['categoria'];
        $requisitos= $this->requisitos($so, $graficos, $espacio);
        // inserta en la DB y redirige
        
        if (($_FILES['imagen']['type'] == "image/jpg" || 
        $_FILES['imagen']['type'] == "image/jpeg" || 
        $_FILES['imagen']['type'] == "image/png")) {
            $success =  $this->traerModeloFichas()->insertarConImagen($titulo, $descripcion, $categoria, $requisitos, $link);
            header("Location: " . BASE_URL . "");
        } else {
            $success = $this->traerModeloFichas()-> insertarFicha($titulo, $descripcion, $categoria, $requisitos, $link);
            header("Location: " . BASE_URL . "");
        }        
            
    }

    public function listarFichas() {
        // pido todos los productos al MODELO
        $fichas = $this->traerModeloFichas()->traerTodasFichas();
        // actualizo la vista
         $this->traerVistaFicha()->mostrarGaleria($fichas);
    }

    public function listadoJuegos( $juego){
        if ($juego=="todos"){
            $fichas = $this->traerModeloFichas()->traerTodasFichas();
            $this->traerVistaFicha()->tablaJuegos($fichas);
        }
    }

//-----------------------------------------------------------------------------------

    public function mostrarJuego($juego){
        $ficha=$this->traerModeloFichas()->traerFicha($juego);
        $this->traerVistaFicha()->mostrarFicha($ficha);
    }

    private function requisitos ($so, $graficos, $espacio){
        $requisito= $so . '/' . $graficos . '/' .$espacio ;
        return $requisito;
    }

    public function eliminarJuego($id_juego) {
        $this->VerificarRegistro();
        $this->traerModeloFichas()->eliminarJuego($id_juego);
        header("Location: " . BASE_URL . "listadojuegos/todos");
    }
         
//-----------------------------------------------------------------------------------

    public function confirmarcambiosjuegos($ficha){
        $this->VerificarRegistro();
        $titulo = $_POST['titulo'];
        $descripcion = $_POST['descripcion'];
        $so = $_POST['so'];
        $graficos = $_POST['grafico'];
        $espacio =$_POST['espacio'];
        $link = $_POST['link'];
        $categoria = $_POST['categoria'];

        $requisitos= $this->requisitos($so, $graficos, $espacio);

        if (($_FILES['imagen']['type'] == "image/jpg" || 
            $_FILES['imagen']['type'] == "image/jpeg" || 
            $_FILES['imagen']['type'] == "image/png")) {
                $success =  $this->traerModeloFichas()->insertarConImagen($titulo, $descripcion, $categoria, $requisitos, $link, $ficha);
                header("Location: " . BASE_URL . "");
        } else {
                $success =$this->traerModeloFichas()-> modificarJuego($titulo, $descripcion, $categoria, $requisitos, $link, $ficha);
                header("Location: " . BASE_URL . "listadojuegos/todos");
            header("Location: " . BASE_URL . "");
        }    
        
    }
}

