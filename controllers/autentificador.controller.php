<?php

require_once 'models/usuario.model.php';
require_once 'proyecto.controler.php';
require_once 'models/categoria.model.php';
require_once 'models/ficha.model.php';//enlazo con archivo de modelo.
require_once 'views/ficha.view.php';//enlazo con archivo de vista.

class ControladorAutentificador extends ControladorPadre {

    
    public function verificar() {
        $email = $_POST['email'];
        $contraseña = $_POST['contraseña'];

        // busco el usuario
        $usuario = $this->traerModeloUsuario()->traerUsuario($email);

        // si existe el usuario y el password es correcto
        if ($usuario && password_verify($contraseña, $usuario->contraseña)) { 

            // abro session y guardo al usuario logueado
            session_start();
            $_SESSION['registrado'] = true;
            $_SESSION['id_usuario'] = $usuario->id_usuario;
            $_SESSION['email'] = $usuario->email;
            $_SESSION['nombre'] = $usuario->nombre;
            $_SESSION['permiso'] = $usuario->permiso;
            
            header("Location: " . BASE_URL . "");
        } else {
            header("Location: " . BASE_URL . "ingresar");
        }
    }

    public function guardarUsuario(){
        $email = $_POST['email'];
        $nombre = $_POST['nombre'];
        $contraseña = $_POST['contraseña'];
        $permiso = 0;

        $hash = password_hash($contraseña, PASSWORD_DEFAULT);
        $usuario = $this->traerModeloUsuario()->traerUsuario($email);
        if ($usuario) {
            echo 'El email ya existe, por favor intentelo de nuevo.';
        } 
        else { 
        $this->traerModeloUsuario()-> guardarUsuario($email, $nombre, $hash, $permiso);
        $this->verificar();
        }
    }

    /**Destruye la session del usuario*/
    public function cerrarSesion() {
        session_start();
        session_destroy();
        header("Location: " . BASE_URL . '');
    }

    public function administrarUsuarios(){
        $usuarios = $this->traerModeloUsuario()->traerUsuarios();
        $this->traerVistaUsuarios()->mostrarUsuarios($usuarios);
    }

    public function cambiarPermisoUsuario($usuario, $permiso){
        $cambiarPermiso=0;
        if ($permiso==0){
            $cambiarPermiso=1;
        }else {
            $cambiarPermiso=0;
        }
        $permiso = $cambiarPermiso;
        $this->traerModeloUsuario()->cambiarPermisoUsuario($permiso, $usuario);
        header("Location: " . BASE_URL . 'administrarUsuarios');
    }

    public function eliminarUsuario($usuario){
        $this->traerModeloUsuario()->eliminarUsuario($usuario);
        header("Location: " . BASE_URL . 'administrarUsuarios');
    }
    


}