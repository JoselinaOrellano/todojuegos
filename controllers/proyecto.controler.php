<?php
require_once 'models/usuario.model.php';
require_once 'models/categoria.model.php';
require_once 'models/ficha.model.php';//enlazo con archivo de modelo.
require_once 'views/ficha.view.php';//enlazo con archivo de vista.
require_once 'views/formulario.view.php';//enlazo con archivo de vista.
require_once 'views/usuarios.view.php';

class ControladorPadre {

    private $modeloUsuario;
    private $modeloCategoria;
    private $modeloFichas;
    private $vistaFicha;
    private $vistaFormulario;
    private $vistaUsuarios;

    public function __construct() {//cuando se llama a la clase el CONSTRUCT realiza una accion por default.
        $this->modeloCategoria = new ModeloCategoria();
        $this->modeloUsuario = new ModeloUsuario();
        $c=$this->modeloCategoria->traerCategorias();
        $usuario=$this->permiso();
        $this->modeloFichas = new ModeloFicha();//inicio las clases dentro de las variables.
        $this->vistaFicha = new VistaFicha($c, $usuario);
        $this->vistaFormulario = new FormularioVista($c, $usuario);
        $this->vistaUsuarios = new VistaUsuarios($c, $usuario);

    }
    private function permiso() {
        session_start(); 
        $user=[
            'nombre'=>'no',
            
            'id_usuario'=>0,
        ];
        if (!isset($_SESSION['registrado'])) {
            $user['nombre']= 'no';
            $permiso['permiso']=0;
          
        }else{
            $user['nombre']= $_SESSION['nombre'];
            $user['id_usuario']= $_SESSION['id_usuario'];
            $user['permiso']= $_SESSION['permiso'];
            
        }
        return $user;
    }
    static private function start() {
        if (session_status() != PHP_SESSION_ACTIVE)
            session_start();
    }
    static public function VerificarRegistro() {
        // llama al metodo estatico privado para iniciar la session
        self::start(); 

        if (!isset($_SESSION['registrado'])) {
            header('Location: ' . BASE_URL . 'ingresar');
            die();
        }
    }

    public function traerModeloCategoria(){
         return $this->modeloCategoria;
    }
    public function traerModeloFichas(){
         return $this->modeloFichas;
    }
    public function traerVistaFicha(){
         return $this->vistaFicha;
    }
    public function traerVistaformulario(){
         return $this->vistaFormulario;
    }
    public function traerModeloUsuario(){
         return $this->modeloUsuario;
    }
    public function traerVistaUsuarios(){
        return $this->vistaUsuarios;
    }
}