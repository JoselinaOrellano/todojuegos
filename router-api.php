<?php
require_once "api/comentario-api.controller.php";
require_once "libs/router/Router.php";

$router = new Router();

//tabla de ruteo
$router->addRoute('comentarios/:id_juego','GET','Comentarios_apiControlador','mostrarComentarios');
$router->addRoute('comentarios/','POST','Comentarios_apiControlador','agregarComentarios');
$router->addRoute('comentarios/:id_comentario','DELETE','Comentarios_apiControlador','eliminarComentario');

//ruteador
$router->route($_REQUEST['resource'], $_SERVER['REQUEST_METHOD']);