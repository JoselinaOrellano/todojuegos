<?php
    define('BASE_URL', '//'.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']).'/');

    require_once 'controllers/ficha.controller.php';

    require_once 'controllers/categoria.controller.php';

    require_once 'controllers/formulario.controller.php';
    
    require_once 'controllers/autentificador.controller.php';
    
    $action= $_REQUEST['action'];
    $urlAction= explode('/',$action);

    if($urlAction[0]==''){
        $urlAction[0]='home';
    }

    switch ($urlAction[0]){
        case 'home': {
           $ctrl= new ControladorFicha();
           $ctrl->listarFichas();
        } break;

//--Mostrar Juegos--------------------------------------------------------------
        
        case 'listadojuegos': {
            $ctrl= new ControladorFicha();
            $juego=$urlAction[1];
            $ctrl ->listadoJuegos( $juego);
        } break;

        case 'fichajuegos':{
            $ctrl= new ControladorFicha();
            $juego=$urlAction[1];
            $ctrl ->mostrarJuego($juego);
        }break;

//--Cargar Juegos-----------------------------------------------------------------------------

        case 'cargarjuego':{
            $ctrl= new ControladorFormulario();
            $ctrl->formularioCargarJuego();
        }break;

        case 'agregar':{
            $ctrl= new ControladorFicha();
            $ctrl->subirFicha();
        }break;

//--Editar Juegos--------------------------------------------------------------------

        case 'modificarjuego':{
            $ctrl= new ControladorFormulario();
            $juego=$urlAction[1];
            $ctrl->formularioModificarJuego($juego);
        }break;

        case 'confirmarcambiosjuegos':{
            $ctrl= new ControladorFicha();
            $juego=$urlAction[1];
            $ctrl->confirmarcambiosjuegos($juego);
        }break;

        case 'eliminarjuego':{
            $ctrl= new ControladorFicha();
            $juego=$urlAction[1];
            $ctrl->eliminarJuego($juego);
        }break;

//--Usuarios----------------------------------------------------------------------------

        case 'ingresar':{
            $ctrl= new ControladorFormulario();
            $ctrl->formularioIngresar();
        }break;

        case 'registrar':{
            $ctrl= new ControladorAutentificador();
            $ctrl->verificar();
        }break;
        
        case 'registrarse':{
            $ctrl= new ControladorFormulario();
            $ctrl->formularioRegistrarse();
        }break;

        case 'guardarUsuario':{
            $ctrl= new ControladorAutentificador();
            $ctrl->guardarUsuario();
        }break;

        case 'cerrarsesion':{
            $ctrl= new ControladorAutentificador();
            $ctrl->cerrarSesion();
        }break;

        case 'administrarUsuarios':{
            $ctrl= new ControladorAutentificador();
            $ctrl->administrarUsuarios();
        }break;

        case 'cambiarpermiso':{
            $ctrl= new ControladorAutentificador();
            $usuario=$urlAction[1];
            $permiso=$urlAction[2];
            $ctrl->cambiarPermisoUsuario($usuario, $permiso);
        }break;

        case 'eliminarUsuario':{
            $ctrl= new ControladorAutentificador();
            $usuario=$urlAction[1];
            $ctrl->eliminarUsuario($usuario);
        }break;

//--Categorias----------------------------------------------------------------       

        case 'cargarcategoria':{
            $ctrl= new ControladorFormulario();
            $ctrl->formularioCargarCategoria();
        }break;

        case 'insertarcategoria':{
            $ctrl= new ControladorCategoria();
            $ctrl->subirCategoria();
        }break;

        case 'modificarcategoria':{
            $id_categoria= $urlAction[1];
            $ctrl= new ControladorFormulario();
            $ctrl->formularioModificarCategoria($id_categoria);
        }break;

        case 'listadoCategorias': {
            $ctrl= new ControladorCategoria();
            $categoria=$urlAction[1];
            $ctrl ->listadoCategorias( $categoria);
        } break;

        case 'eliminarcategoria':{
            $ctrl= new ControladorCategoria();
            $categoria=$urlAction[1];
            
            $ctrl->eliminarCategoria($categoria);
        }break;

        case 'confirmarcambioscategoria':{
            $ctrl= new ControladorCategoria();
            $categoria=$urlAction[1];
            $ctrl->confirmarCambiosCategoria($categoria);
        }break;
    }
   