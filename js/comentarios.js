"use strict"

let app = new Vue({
    el: "#appVue",
    data: {
        id_juego: 0,
        id_usuario: 0,
        usuario: '...',
        comentarios: [],
        registro:0,
    },
    methods:{
        enviar: function(e){
            agregarComentario();
        },
        eliminarComentario: function(comentario) {
            let url = 'api/comentarios/' + comentario;
            fetch(url, {
                method: 'DELETE',
                
            })
            traerComentarios();
        }
    }
});


app.id_juego = document.getElementById("id_juego").value;
app.id_usuario = document.getElementById("dato").value;
app.usuario = document.getElementById("dato2").value;
app.registro = document.getElementById("registro").value;

traerComentarios();




function agregarComentario() {
    let contenido = document.getElementById("contenido").value;
    let puntaje = document.getElementById("puntaje").value;

    if (contenido == ' ') {
        alert("Debe ingresar un comentario valido");
    } else {
        let data = {
            "comentario": contenido,
            "puntaje": puntaje,
            "id_usuario": app.id_usuario,
            "id_juego": app.id_juego,

        }
        fetch('api/comentarios', {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'aplication/json'
            }
        })
    }
    traerComentarios();
}

function traerComentarios() {
    fetch("api/comentarios/" + app.id_juego)
        .then(response => response.json())
        .then(json => {
            // asigno las tareas que me devuelve la API
            app.comentarios = json; // es como el $this->smarty->assign("tasks", tareas);

        });
}