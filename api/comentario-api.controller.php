<?php
require_once 'models/comentario.model.php';
require_once 'api/comentario-api.view.php';

class Comentarios_apiControlador{    
    private $model;
    private $view;  
    private $data;

    public function __construct(){
        $this->model = new ComentariosModelo();
        $this->view = new Comentarios_apiVista();
        $this->data = file_get_contents("php://input");
    }
    public function mostrarComentarios($params = []){
       $comentarios = $this->model-> obtenerComentarios($params[':id_juego']);
       $this->view->response($comentarios, 200);
    }

    public function agregarComentarios(){
        $datos = $this-> transformarDatos();
        $juego = $datos->id_juego;
        $usuario = $datos->id_usuario;
        $comentario = $datos->comentario;
        $puntaje = $datos->puntaje;

        $this->model->agregarComentario($juego, $usuario, $comentario, $puntaje);
    }

    public function transformardatos(){
        return json_decode($this->data);
    }

    public function eliminarComentario($params = []) {     
        $response=$this->model->eliminarComentario($params[':id_comentario']);
        if ($response){
             $this->view->response($response, 200);
        } else {
            $response="Error al eliminar el comentario";
            $this->view->response($response, 500);
        }        
    }
}