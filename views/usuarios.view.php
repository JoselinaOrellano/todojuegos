<?php
require_once('libs/smarty/Smarty.class.php');
require_once('constructor.view.php');

class VistaUsuarios extends ConstructorVista{

    public function mostrarUsuarios($usuarios){
        $this->traerSmarty()->assign('usuarios', $usuarios);
        $this->traerSmarty()->display('usuarios.tpl');
    }


}
